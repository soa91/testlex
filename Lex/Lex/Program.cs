﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lex
{
    class Program
    {
        static void Main(string[] args)
        {
            string query = "покажи блок подключеный к разьему а1";

            var tokenizer = Tokenizer.Tokenize(query);

            foreach (var dslToken in tokenizer)
            {
                Console.WriteLine("{0}, \t{1}", dslToken.TokenType.ToString(), dslToken.Value);
            }
        }
    }
}
