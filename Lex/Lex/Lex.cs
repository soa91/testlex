﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lex
{
    #region разбиение строки на токены
    public enum TokenType
    {
        FnShow,
        OpConnect,
        OpInclude,
        OpBlock,
        OpConnector,
        OpContact,
        OpWire,
        SequenceTerminator,
        StringValue
    }

    public static class Tokenizer
    {
        private static readonly List<TokenDefinition> TokenDefinitions = new List<TokenDefinition>();

        static Tokenizer()
        {
            TokenDefinitions.Clear();

            //TODO: поправить на враианты без окончаний

            //функции
            TokenDefinitions.Add(new TokenDefinition(TokenType.FnShow, @"^покажи"));
            TokenDefinitions.Add(new TokenDefinition(TokenType.OpConnect, @"^соединен\S*\b|^связан\S*\b|^подключен\S*\b"));
            TokenDefinitions.Add(new TokenDefinition(TokenType.OpInclude, @"^содержит"));
            //тип
            TokenDefinitions.Add(new TokenDefinition(TokenType.OpBlock, @"^блок"));
            TokenDefinitions.Add(new TokenDefinition(TokenType.OpConnector, @"^разъем"));
            TokenDefinitions.Add(new TokenDefinition(TokenType.OpContact, @"^контакт"));
            TokenDefinitions.Add(new TokenDefinition(TokenType.OpWire, @"^кабель"));
            TokenDefinitions.Add(new TokenDefinition(TokenType.StringValue, @"^\S*\b"));
        }

        public static List<DslToken> Tokenize(string lqlText)
        {
            var tokens = new List<DslToken>();
            string remainingText = lqlText;

            while (!string.IsNullOrWhiteSpace(remainingText))
            {
                var match = FindMatch(remainingText);
                if (match.IsMatch)
                {
                    tokens.Add(new DslToken(match.TokenType, match.Value));
                    remainingText = match.RemainingText;
                }
                else
                {
                    remainingText = remainingText.Substring(1);
                }
            }

            tokens.Add(new DslToken(TokenType.SequenceTerminator, string.Empty));

            return tokens;
        }

        private static TokenMatch FindMatch(string lqlText)
        {
            foreach (var tokenDefinition in TokenDefinitions)
            {
                var match = tokenDefinition.Match(lqlText);
                if (match.IsMatch)
                    return match;
            }

            return new TokenMatch() { IsMatch = false };
        }
    }

    public class TokenDefinition
    {
        private Regex _regex;
        private readonly TokenType _returnsToken;

        public TokenDefinition(TokenType returnsToken, string regexPattern)
        {
            _regex = new Regex(regexPattern, RegexOptions.IgnoreCase);
            _returnsToken = returnsToken;
        }

        public TokenMatch Match(string inputString)
        {
            var match = _regex.Match(inputString);
            if (match.Success)
            {
                string remainingText = string.Empty;
                if (match.Length != inputString.Length)
                    remainingText = inputString.Substring(match.Length);

                return new TokenMatch()
                {
                    IsMatch = true,
                    RemainingText = remainingText,
                    TokenType = _returnsToken,
                    Value = match.Value
                };
            }
            else
            {
                return new TokenMatch() { IsMatch = false };
            }

        }
    }

    public class TokenMatch
    {
        public bool IsMatch { get; set; }
        public TokenType TokenType { get; set; }
        public string Value { get; set; }
        public string RemainingText { get; set; }
    }

    public class DslToken
    {
        public DslToken(TokenType tokenType)
        {
            TokenType = tokenType;
            Value = string.Empty;
        }

        public DslToken(TokenType tokenType, string value)
        {
            TokenType = tokenType;
            Value = value;
        }

        public TokenType TokenType { get; set; }
        public string Value { get; set; }

        public DslToken Clone()
        {
            return new DslToken(TokenType, Value);
        }
    }
    #endregion
}
